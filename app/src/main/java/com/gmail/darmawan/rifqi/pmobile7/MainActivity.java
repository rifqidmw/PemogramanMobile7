package com.gmail.darmawan.rifqi.pmobile7;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.BatteryManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {
    private TextView mBatteryLevelText, mStatusBattery, mHealthBattery, mNetwork;
    private ProgressBar mBatteryLevelProgress;
    private BroadcastReceiver mReceiver;
    private PowerConnectionReceiver pReceiver;
    private TelephonyManager telephonyManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mNetwork = (TextView) findViewById(R.id.txt_network);
        mHealthBattery = (TextView) findViewById(R.id.txt_health);
        mStatusBattery = (TextView) findViewById(R.id.txt_status);
        mBatteryLevelText = (TextView) findViewById(R.id.txt_bat);
        mBatteryLevelProgress = (ProgressBar) findViewById(R.id.progressBar);
        mReceiver = new BatteryBroadcastReceiver();

        telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        int networkType = telephonyManager.getNetworkType();
        switch (networkType){
            case TelephonyManager.NETWORK_TYPE_GPRS:
                mNetwork.setText("2G");
                break;
            case TelephonyManager.NETWORK_TYPE_EDGE:
                mNetwork.setText("2G");
                break;
            case TelephonyManager.NETWORK_TYPE_CDMA:
                mNetwork.setText("2G");
                break;
            case TelephonyManager.NETWORK_TYPE_UMTS:
                mNetwork.setText("3G");
                break;
            case TelephonyManager.NETWORK_TYPE_EVDO_0:
                mNetwork.setText("3G");
                break;
            case TelephonyManager.NETWORK_TYPE_EVDO_A:
                mNetwork.setText("3G");
                break;
            case TelephonyManager.NETWORK_TYPE_HSDPA:
                mNetwork.setText("3G");
                break;
            case TelephonyManager.NETWORK_TYPE_HSUPA:
                mNetwork.setText("3G");
                break;
            case TelephonyManager.NETWORK_TYPE_HSPA:
                mNetwork.setText("3G");
                break;
            case TelephonyManager.NETWORK_TYPE_EVDO_B:
                mNetwork.setText("3G");
                break;
            case TelephonyManager.NETWORK_TYPE_EHRPD:
                mNetwork.setText("3G");
                break;
            case TelephonyManager.NETWORK_TYPE_HSPAP:
                mNetwork.setText("3G");
                break;
            case TelephonyManager.NETWORK_TYPE_LTE:
                mNetwork.setText("4G");
                break;
            default:
                mNetwork.setText("Unknown");
                break;
        }
    }

    public String getNetworkClass(Context context) {
        TelephonyManager mTelephonyManager = (TelephonyManager)
                context.getSystemService(Context.TELEPHONY_SERVICE);
        int networkType = mTelephonyManager.getNetworkType();
        switch (networkType) {
            case TelephonyManager.NETWORK_TYPE_GPRS:
            case TelephonyManager.NETWORK_TYPE_EDGE:
            case TelephonyManager.NETWORK_TYPE_CDMA:
            case TelephonyManager.NETWORK_TYPE_1xRTT:
            case TelephonyManager.NETWORK_TYPE_IDEN:
                return "2G";
            case TelephonyManager.NETWORK_TYPE_UMTS:
            case TelephonyManager.NETWORK_TYPE_EVDO_0:
            case TelephonyManager.NETWORK_TYPE_EVDO_A:
            case TelephonyManager.NETWORK_TYPE_HSDPA:
            case TelephonyManager.NETWORK_TYPE_HSUPA:
            case TelephonyManager.NETWORK_TYPE_HSPA:
            case TelephonyManager.NETWORK_TYPE_EVDO_B:
            case TelephonyManager.NETWORK_TYPE_EHRPD:
            case TelephonyManager.NETWORK_TYPE_HSPAP:
                mNetwork.setText(networkType);
                return "3G";
            case TelephonyManager.NETWORK_TYPE_LTE:
                mNetwork.setText(networkType);
                return "4G";
            default:
                mNetwork.setText(networkType);
                return "Unknown";
        }
    }

    @Override
    protected void onStart() {
        registerReceiver(mReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        super.onStart();
    }

    @Override
    protected void onStop() {
        unregisterReceiver(mReceiver);
        super.onStop();
    }

    @Override
    protected void onResume(){
        super.onResume();
        pReceiver = new PowerConnectionReceiver();

        IntentFilter ifilter = new IntentFilter();
        ifilter.addAction(Intent.ACTION_POWER_CONNECTED);
        ifilter.addAction(Intent.ACTION_POWER_DISCONNECTED);
        registerReceiver(pReceiver, ifilter);
    }

    @Override
    protected void onPause(){
        super.onPause();
        unregisterReceiver(pReceiver);
    }
    private class BatteryBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);

            mBatteryLevelText.setText(level + " %");
            mBatteryLevelProgress.setProgress(level);

            int health = intent.getIntExtra(BatteryManager.EXTRA_HEALTH, -1);
            switch (health){
                case BatteryManager.BATTERY_HEALTH_COLD:
                    mHealthBattery.setText("Dingin");
                    break;
                case BatteryManager.BATTERY_HEALTH_DEAD:
                    mHealthBattery.setText("Mati");
                    break;
                case BatteryManager.BATTERY_HEALTH_GOOD:
                    mHealthBattery.setText("Bagus");
                    break;
                case BatteryManager.BATTERY_HEALTH_OVERHEAT:
                    mHealthBattery.setText("Terlalu Panas");
                    break;
                case BatteryManager.BATTERY_HEALTH_OVER_VOLTAGE:
                    mHealthBattery.setText("Tegangan terlalu tinggi");
                    break;
                case BatteryManager.BATTERY_HEALTH_UNKNOWN:
                    mHealthBattery.setText("Tidak diketahui");
                    break;
                case BatteryManager.BATTERY_HEALTH_UNSPECIFIED_FAILURE:
                    mHealthBattery.setText("Gagal mengetahui");
                    break;
            }
        }
    }

    public void playAudio(View view){
        Intent objIntent = new Intent (this, PlayAudio.class);
        startService(objIntent);
    }

    public void stopAudio(View view){
        Intent objIntent = new Intent (this, StopAudio.class);
        startService(objIntent);
    }
    public class PowerConnectionReceiver extends BroadcastReceiver {

        public PowerConnectionReceiver() {
        }

        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals(Intent.ACTION_POWER_CONNECTED)) {
                mStatusBattery.setText("Sedang mengisi baterai");
            } else {
                intent.getAction().equals(Intent.ACTION_POWER_DISCONNECTED);
                mStatusBattery.setText("Sedang tidak mengisi baterai");
            }
        }
    }

}
